<?php

namespace App\Repository;

use App\Entity\PagarServico;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PagarServico|null find($id, $lockMode = null, $lockVersion = null)
 * @method PagarServico|null findOneBy(array $criteria, array $orderBy = null)
 * @method PagarServico[]    findAll()
 * @method PagarServico[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PagarServicoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PagarServico::class);
    }

    // /**
    //  * @return PagarServico[] Returns an array of PagarServico objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PagarServico
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
