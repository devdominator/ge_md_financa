<?php

namespace App\Repository;

use App\Entity\PagarActividade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PagarActividade|null find($id, $lockMode = null, $lockVersion = null)
 * @method PagarActividade|null findOneBy(array $criteria, array $orderBy = null)
 * @method PagarActividade[]    findAll()
 * @method PagarActividade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PagarActividadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PagarActividade::class);
    }

    // /**
    //  * @return PagarActividade[] Returns an array of PagarActividade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PagarActividade
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
