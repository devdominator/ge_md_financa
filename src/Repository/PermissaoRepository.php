<?php

namespace App\Repository;

use App\Entity\Permissao;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Permissao|null find($id, $lockMode = null, $lockVersion = null)
 * @method Permissao|null findOneBy(array $criteria, array $orderBy = null)
 * @method Permissao[]    findAll()
 * @method Permissao[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermissaoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Permissao::class);
    }

    // /**
    //  * @return Permissao[] Returns an array of Permissao objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Permissao
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
