<?php

namespace App\Repository;

use App\Entity\Mensalidade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mensalidade|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mensalidade|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mensalidade[]    findAll()
 * @method Mensalidade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MensalidadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mensalidade::class);
    }

    // /**
    //  * @return Mensalidade[] Returns an array of Mensalidade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mensalidade
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
