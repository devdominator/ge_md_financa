<?php

namespace App\Repository;

use App\Entity\Actividade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Actividade|null find($id, $lockMode = null, $lockVersion = null)
 * @method Actividade|null findOneBy(array $criteria, array $orderBy = null)
 * @method Actividade[]    findAll()
 * @method Actividade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActividadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Actividade::class);
    }

    // /**
    //  * @return Actividade[] Returns an array of Actividade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Actividade
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
