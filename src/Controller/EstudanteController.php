<?php

namespace App\Controller;

use App\Entity\Estudante;
use App\Form\EstudanteType;
use App\Repository\EstudanteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/estudante")
 */
class EstudanteController extends AbstractController
{
    /**
     * @Route("/", name="estudante_index", methods={"GET"})
     */
    public function index(EstudanteRepository $estudanteRepository): Response
    {
        return $this->render('estudante/index.html.twig', [
            'estudantes' => $estudanteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="estudante_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $estudante = new Estudante();
        $form = $this->createForm(EstudanteType::class, $estudante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($estudante);
            $entityManager->flush();

            return $this->redirectToRoute('estudante_index');
        }

        return $this->render('estudante/new.html.twig', [
            'estudante' => $estudante,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="estudante_show", methods={"GET"})
     */
    public function show(Estudante $estudante): Response
    {
        return $this->render('estudante/show.html.twig', [
            'estudante' => $estudante,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="estudante_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Estudante $estudante): Response
    {
        $form = $this->createForm(EstudanteType::class, $estudante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('estudante_index');
        }

        return $this->render('estudante/edit.html.twig', [
            'estudante' => $estudante,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="estudante_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Estudante $estudante): Response
    {
        if ($this->isCsrfTokenValid('delete'.$estudante->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($estudante);
            $entityManager->flush();
        }

        return $this->redirectToRoute('estudante_index');
    }
}
