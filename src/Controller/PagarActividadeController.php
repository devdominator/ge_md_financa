<?php

namespace App\Controller;

use App\Entity\PagarActividade;
use App\Form\PagarActividadeType;
use App\Repository\PagarActividadeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pagar/actividade")
 */
class PagarActividadeController extends AbstractController
{
    /**
     * @Route("/", name="pagar_actividade_index", methods={"GET"})
     */
    public function index(PagarActividadeRepository $pagarActividadeRepository): Response
    {
        return $this->render('pagar_actividade/index.html.twig', [
            'pagar_actividades' => $pagarActividadeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pagar_actividade_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pagarActividade = new PagarActividade();
        $form = $this->createForm(PagarActividadeType::class, $pagarActividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pagarActividade);
            $entityManager->flush();

            return $this->redirectToRoute('pagar_actividade_index');
        }

        return $this->render('pagar_actividade/new.html.twig', [
            'pagar_actividade' => $pagarActividade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pagar_actividade_show", methods={"GET"})
     */
    public function show(PagarActividade $pagarActividade): Response
    {
        return $this->render('pagar_actividade/show.html.twig', [
            'pagar_actividade' => $pagarActividade,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pagar_actividade_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PagarActividade $pagarActividade): Response
    {
        $form = $this->createForm(PagarActividadeType::class, $pagarActividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pagar_actividade_index');
        }

        return $this->render('pagar_actividade/edit.html.twig', [
            'pagar_actividade' => $pagarActividade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pagar_actividade_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PagarActividade $pagarActividade): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pagarActividade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pagarActividade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pagar_actividade_index');
    }
}
