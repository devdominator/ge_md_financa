<?php

namespace App\Controller;

use App\Entity\Actividade;
use App\Form\ActividadeType;
use App\Repository\ActividadeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/actividade")
 */
class ActividadeController extends AbstractController
{
    /**
     * @Route("/", name="actividade_index", methods={"GET"})
     */
    public function index(ActividadeRepository $actividadeRepository): Response
    {
        return $this->render('actividade/index.html.twig', [
            'actividades' => $actividadeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="actividade_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $actividade = new Actividade();
        $form = $this->createForm(ActividadeType::class, $actividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($actividade);
            $entityManager->flush();

            return $this->redirectToRoute('actividade_index');
        }

        return $this->render('actividade/new.html.twig', [
            'actividade' => $actividade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="actividade_show", methods={"GET"})
     */
    public function show(Actividade $actividade): Response
    {
        return $this->render('actividade/show.html.twig', [
            'actividade' => $actividade,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="actividade_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Actividade $actividade): Response
    {
        $form = $this->createForm(ActividadeType::class, $actividade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('actividade_index');
        }

        return $this->render('actividade/edit.html.twig', [
            'actividade' => $actividade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="actividade_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Actividade $actividade): Response
    {
        if ($this->isCsrfTokenValid('delete'.$actividade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($actividade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('actividade_index');
    }
}
