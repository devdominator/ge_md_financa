<?php

namespace App\Controller;

use App\Entity\SolicitarDocumento;
use App\Form\SolicitarDocumentoType;
use App\Repository\SolicitarDocumentoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/solicitar/documento")
 */
class SolicitarDocumentoController extends AbstractController
{
    /**
     * @Route("/", name="solicitar_documento_index", methods={"GET"})
     */
    public function index(SolicitarDocumentoRepository $solicitarDocumentoRepository): Response
    {
        return $this->render('solicitar_documento/index.html.twig', [
            'solicitar_documentos' => $solicitarDocumentoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="solicitar_documento_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $solicitarDocumento = new SolicitarDocumento();
        $form = $this->createForm(SolicitarDocumentoType::class, $solicitarDocumento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($solicitarDocumento);
            $entityManager->flush();

            return $this->redirectToRoute('solicitar_documento_index');
        }

        return $this->render('solicitar_documento/new.html.twig', [
            'solicitar_documento' => $solicitarDocumento,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="solicitar_documento_show", methods={"GET"})
     */
    public function show(SolicitarDocumento $solicitarDocumento): Response
    {
        return $this->render('solicitar_documento/show.html.twig', [
            'solicitar_documento' => $solicitarDocumento,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="solicitar_documento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SolicitarDocumento $solicitarDocumento): Response
    {
        $form = $this->createForm(SolicitarDocumentoType::class, $solicitarDocumento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('solicitar_documento_index');
        }

        return $this->render('solicitar_documento/edit.html.twig', [
            'solicitar_documento' => $solicitarDocumento,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="solicitar_documento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SolicitarDocumento $solicitarDocumento): Response
    {
        if ($this->isCsrfTokenValid('delete'.$solicitarDocumento->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($solicitarDocumento);
            $entityManager->flush();
        }

        return $this->redirectToRoute('solicitar_documento_index');
    }
}
