<?php

namespace App\Controller;

use App\Entity\Utilizador;
use App\Form\UtilizadorType;
use App\Repository\UtilizadorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
 use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/utilizador")
 */
class UtilizadorController extends AbstractController
{
    /**
     * @Route("/", name="utilizador_index", methods={"GET"})
     */
    public function index(UtilizadorRepository $utilizadorRepository): Response
    {
        return $this->render('utilizador/index.html.twig', [
            'utilizadors' => $utilizadorRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="utilizador_new", methods={"GET","POST"})
     */
        public function new(Request $request, UserPasswordEncoderInterface $coder): Response
    {
        $utilizador = new Utilizador();
        $form = $this->createForm(UtilizadorType::class, $utilizador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $utilizador->setPassword($coder->encodePassword($utilizador, $utilizador->getPassword()));
            $entityManager->persist($utilizador);
            $entityManager->flush();

            return $this->redirectToRoute('utilizador_index');
        }

        return $this->render('utilizador/new.html.twig', [
            'utilizador' => $utilizador,
            'form' => $form->createView(),
            'edit' => false,
        ]);
    }

    /**
     * @Route("/{id}", name="utilizador_show", methods={"GET"})
     */
    public function show(Utilizador $utilizador): Response
    {
        return $this->render('utilizador/show.html.twig', [
            'utilizador' => $utilizador,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="utilizador_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Utilizador $utilizador, UserPasswordEncoderInterface $coder): Response
    {
        
        $form = $this->createForm(UtilizadorType::class, $utilizador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            
//            $utilizador->setPassword($coder->encodePassword($utilizador, $utilizador->getPassword()));
//            $manager->persist($utilizador);
            $manager->flush();
            return $this->redirectToRoute('utilizador_index');
        }

        return $this->render('utilizador/edit.html.twig', [
            'utilizador' => $utilizador,
            'form' => $form->createView(),
            'edit' => true
        ]);
    }

    /**
     * @Route("/{id}", name="utilizador_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Utilizador $utilizador): Response
    {
        if ($this->isCsrfTokenValid('delete'.$utilizador->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($utilizador);
            $entityManager->flush();
        }

        return $this->redirectToRoute('utilizador_index');
    }
}
