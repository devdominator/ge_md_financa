<?php

namespace App\Controller;

use App\Repository\EstudanteRepository;
use App\Repository\UtilizadorRepository;
use App\Repository\ActividadeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(EstudanteRepository $er, UtilizadorRepository $ur, ActividadeRepository $ar): Response
    {
        
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'tmUser' => count($ur->findAll()),
            'tmActi' => count($er->findAll()),
            'tmAlu' => count($ar->findAll()),
        ]);
    }
}
