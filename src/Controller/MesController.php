<?php

namespace App\Controller;

use App\Entity\Mes;
use App\Form\MesType;
use App\Repository\MesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mes")
 */
class MesController extends AbstractController
{
    /**
     * @Route("/", name="mes_index", methods={"GET"})
     */
    public function index(MesRepository $mesRepository): Response
    {
        return $this->render('mes/index.html.twig', [
            'mes' => $mesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="mes_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $me = new Mes();
        $form = $this->createForm(MesType::class, $me);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($me);
            $entityManager->flush();

            return $this->redirectToRoute('mes_index');
        }

        return $this->render('mes/new.html.twig', [
            'me' => $me,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mes_show", methods={"GET"})
     */
    public function show(Mes $me): Response
    {
        return $this->render('mes/show.html.twig', [
            'me' => $me,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mes_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mes $me): Response
    {
        $form = $this->createForm(MesType::class, $me);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mes_index');
        }

        return $this->render('mes/edit.html.twig', [
            'me' => $me,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mes_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Mes $me): Response
    {
        if ($this->isCsrfTokenValid('delete'.$me->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($me);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mes_index');
    }
}
