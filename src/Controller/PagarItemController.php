<?php

namespace App\Controller;

use App\Entity\PagarItem;
use App\Form\PagarItemType;
use App\Repository\PagarItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pagar/item")
 */
class PagarItemController extends AbstractController
{
    /**
     * @Route("/", name="pagar_item_index", methods={"GET"})
     */
    public function index(PagarItemRepository $pagarItemRepository): Response
    {
        return $this->render('pagar_item/index.html.twig', [
            'pagar_items' => $pagarItemRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pagar_item_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pagarItem = new PagarItem();
        $form = $this->createForm(PagarItemType::class, $pagarItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pagarItem);
            $entityManager->flush();

            return $this->redirectToRoute('pagar_item_index');
        }

        return $this->render('pagar_item/new.html.twig', [
            'pagar_item' => $pagarItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pagar_item_show", methods={"GET"})
     */
    public function show(PagarItem $pagarItem): Response
    {
        return $this->render('pagar_item/show.html.twig', [
            'pagar_item' => $pagarItem,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pagar_item_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PagarItem $pagarItem): Response
    {
        $form = $this->createForm(PagarItemType::class, $pagarItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pagar_item_index');
        }

        return $this->render('pagar_item/edit.html.twig', [
            'pagar_item' => $pagarItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pagar_item_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PagarItem $pagarItem): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pagarItem->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pagarItem);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pagar_item_index');
    }
}
