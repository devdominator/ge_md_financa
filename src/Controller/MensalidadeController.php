<?php

namespace App\Controller;

use App\Entity\Mensalidade;
use App\Form\MensalidadeType;
use App\Repository\MensalidadeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mensalidade")
 */
class MensalidadeController extends AbstractController {

    /**
     * @Route("/", name="mensalidade_index", methods={"GET"})
     */
    public function index(MensalidadeRepository $mensalidadeRepository): Response {
        return $this->render('mensalidade/index.html.twig', [
                    'mensalidades' => $mensalidadeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="mensalidade_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $mensalidade = new Mensalidade();
        $form = $this->createForm(MensalidadeType::class, $mensalidade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            if ($mensalidade->getValor() >= $mensalidade->getEstudante()->getClasse()->getPreco()) {
                $entityManager->persist($mensalidade);
                $entityManager->flush();

                return $this->redirectToRoute('mensalidade_index');
            } else {
                return $this->render('mensalidade/new.html.twig', [
                            'mensalidade' => $mensalidade,
                            'form' => $form->createView(),
                            'erro' => "O valor inserido é inferior. O valor tem que ser igual ou superior a " . $mensalidade->getEstudante()->getClasse()->getPreco() . " Kz"
                ]);
            }
        }

        return $this->render('mensalidade/new.html.twig', [
                    'mensalidade' => $mensalidade,
                    'form' => $form->createView(),
                    'erro' => null
        ]);
    }

    /**
     * @Route("/{id}", name="mensalidade_show", methods={"GET"})
     */
    public function show(Mensalidade $mensalidade): Response {
        return $this->render('mensalidade/show.html.twig', [
                    'mensalidade' => $mensalidade,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mensalidade_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mensalidade $mensalidade): Response {
        $form = $this->createForm(MensalidadeType::class, $mensalidade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mensalidade_index');
        }

        return $this->render('mensalidade/edit.html.twig', [
                    'mensalidade' => $mensalidade,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mensalidade_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Mensalidade $mensalidade): Response {
        if ($this->isCsrfTokenValid('delete' . $mensalidade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mensalidade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mensalidade_index');
    }

}
