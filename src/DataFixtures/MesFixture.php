<?php

namespace App\DataFixtures;

use App\Entity\Mes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MesFixture extends Fixture {

    public function load(ObjectManager $manager) {
        // $product = new Product();
        // $manager->persist($product);

        $meses = [
            ["Janeiro", 1, 0],
            ["Fevereiro", 2, 1],
            ["Março", 3, 1],
            ["Abril", 4, 1],
            ["Maio", 5, 1],
            ["Junho", 6, 1],
            ["Julho", 7, 1],
            ["Agosto", 8, 1],
            ["Setembro", 9, 1],
            ["Outubro", 10, 1],
            ["Novembro", 11, 1],
            ["Dezembro", 12, 0],
        ];

        foreach ($meses as $m) {
            $mes = new Mes();
            $mes->setNome($m[0]);
            $mes->setOrdem($m[1]);
            $mes->setIsLectivo($m[2]);

            $manager->persist($mes);
        }
        $manager->flush();
    }

}
