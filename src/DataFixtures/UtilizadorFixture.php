<?php

namespace App\DataFixtures;

use App\Entity\Utilizador;
use App\Entity\Permissao;
use App\Repository\PermissaoRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UtilizadorFixture extends Fixture {

    private $encoder;
    private $pr;

    public function __construct(UserPasswordEncoderInterface $encoder, PermissaoRepository $pr) {
        $this->encoder = $encoder;
        $this->pr = $pr;
    }

    public function load(ObjectManager $manager) {
        // $product = new Product();
        // $manager->persist($product);

        $user = new Utilizador();
        $user->setEmail("rosarialonguenda9@gmail.com");
        $user->setNome("Rosária");
        $user->setPassword($this->encoder->encodePassword($user, "rosaria"));
        $permissao = $this->pr->findOneBy(array('nome' => 'ROLE_ADMIN'));
        $user->setPermissao($permissao);
        $manager->persist($user);

        $manager->flush();
    }

}
