<?php

namespace App\DataFixtures;

use App\Entity\Classe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ClasseFixture extends Fixture {

    public function load(ObjectManager $manager) {
        // $product = new Product();
        // $manager->persist($product);

        $classes = [
            ["1ª Classe", 1, 1000],
            ["2ª Classe", 2, 1500],
            ["3ª Classe", 3, 2000],
            ["4ª Classe", 4, 2500],
            ["5ª Classe", 5, 3000],
            ["6ª Classe", 6, 3500],
            ["7ª Classe", 7, 4000],
            ["8ª Classe", 2, 4500],
            ["9ª Classe", 9, 5000],
        ];

        foreach ($classes as $s) {
            $classe = new Classe();
            $classe->setNome($s[0]);
            $classe->setPreco($s[2]);
            $classe->setGrau($s[1]);
            $manager->persist($classe);
        }
        $manager->flush();
    }

}
