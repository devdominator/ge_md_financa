<?php

namespace App\DataFixtures;

use App\Entity\Permissao;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        
        $permissao0 = new Permissao();
        $permissao0->setNome('ROLE_ADMIN');
        $permissao0->setDescricao("DIRECTOR");
        $manager->persist($permissao0);
        
        $permissao1 = new Permissao();
        $permissao1->setNome('ROLE_SECRETARIO');
        $permissao1->setDescricao("SECRETARIO/DP");
        $manager->persist($permissao1);
        
        $manager->flush();
    }
}
