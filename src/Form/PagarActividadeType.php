<?php

namespace App\Form;

use App\Entity\PagarActividade;
use App\Entity\Estudante;
use App\Entity\Actividade;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PagarActividadeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('actividade', EntityType::class, [
                    'class' => Actividade::class,
                    'choice_label' => function (Actividade $a) {
                        return $a->getDescricao();
                    }
                ])
            ->add('estudante', EntityType::class, [
                    'class' => Estudante::class,
                    'choice_label' => function (Estudante $e) {
                        return $e->getNome()." ".$e->getNomeFamilia()." --> ".$e->getId();
                    }
                ])
            ->add('valor')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PagarActividade::class,
        ]);
    }
}
