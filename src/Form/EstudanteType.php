<?php

namespace App\Form;

use App\Entity\Estudante;
use App\Entity\Classe;
use App\Entity\Responsavel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstudanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome')
            ->add('nomeFamilia')
            ->add('genero', ChoiceType::class, [
                'choices' => [
                    'Feminino' => 'F',
                    'Masculino' => 'M',
                ],
            ])
            ->add('morada')
            ->add('telefone')
            ->add('dataNasc', DateType::class,[
                'widget' => 'single_text',
                'years' => range(0, 99),
            ])
            ->add('numeroBi')
            ->add('classe', EntityType::class, [
                'class' => Classe::class,
                'choice_label' => function (Classe $c) {
                    return $c->getNome();
                },
            ])
            ->add('responsavel', EntityType::class, [
                'class' => Responsavel::class,
                'choice_label' => function (Responsavel $r) {
                    return $r->getNome().' --> '.$r->getTelefone();
                },
                'placeholder'=>'Selecione o Responsável',
                'required'=>false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Estudante::class,
        ]);
    }
}
