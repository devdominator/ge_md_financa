<?php

namespace App\Form;

use App\Entity\PagarItem;
use App\Entity\Item;
use App\Entity\Estudante;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PagarItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estudante', EntityType::class, [
                    'class' => Estudante::class,
                    'choice_label' => function (Estudante $e) {
                        return $e->getNome()." ".$e->getNomeFamilia()." --> ".$e->getId();
                    }
                ])
            ->add('item', EntityType::class, [
                    'class' => Item::class,
                    'choice_label' => function (Item $i) {
                        return $i->getNome();
                    }
                ])
            ->add('valor')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PagarItem::class,
        ]);
    }
}
