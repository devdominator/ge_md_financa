<?php

namespace App\Form;

use App\Entity\Estudante;
use App\Entity\Mensalidade;
use App\Entity\Mes;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MensalidadeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('borderaux')
                ->add('valor')
                ->add('mes', EntityType::class, [
                    'class' => Mes::class,
                    'choice_label' => function (Mes $m) {
                        return $m->getNome();
                    },
                    "query_builder" => function(\App\Repository\MesRepository $mr) {
                        return $mr->createQueryBuilder("m")->where('m.isLectivo = true')->orderBy("m.ordem", "ASC");
                    }
                ])
                ->add('estudante', EntityType::class, [
                    'class' => Estudante::class,
                    'choice_label' => function (Estudante $e) {
                        return $e->getNome() . " " . $e->getNomeFamilia() . " --> " . $e->getId();
                    },
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Mensalidade::class,
        ]);
    }

}
