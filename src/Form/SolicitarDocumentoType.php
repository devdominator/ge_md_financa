<?php

namespace App\Form;

use App\Entity\SolicitarDocumento;
use App\Entity\Documento;
use App\Entity\Estudante;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SolicitarDocumentoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('documento', EntityType::class, [
                'class' => Documento::class,
                'choice_label' => function (Documento $d) {
                    return $d->getNome();
                },
            ])
            ->add('estudante', EntityType::class, [
                'class' => Estudante::class,
                'choice_label' => function (Estudante $e) {
                    return $e->getNome()." ".$e->getNomeFamilia()." --> ".$e->getId();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SolicitarDocumento::class,
        ]);
    }
}
