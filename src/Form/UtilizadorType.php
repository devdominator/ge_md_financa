<?php

namespace App\Form;

use App\Entity\Utilizador;
use App\Entity\Permissao;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilizadorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password')
            ->add('nome')
            ->add('permissao', EntityType::class, [
                    'class' => Permissao::class,
                    'choice_label' => function (Permissao $p) {
                        return $p->getDescricao();
                    }
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilizador::class,
        ]);
    }
}
