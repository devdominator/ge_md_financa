<?php

namespace App\Entity;

use App\Repository\EstudanteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EstudanteRepository::class)
 */
class Estudante
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $nomeFamilia;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $genero;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $morada;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $telefone;

    /**
     * @ORM\Column(type="date")
     */
    private $dataNasc;

    /**
     * @ORM\OneToOne(targetEntity=Classe::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $classe;

    /**
     * @ORM\ManyToOne(targetEntity=Responsavel::class, inversedBy="estudantes")
     */
    private $responsavel;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $numeroBi;

    /**
     * @ORM\OneToMany(targetEntity=PagarItem::class, mappedBy="Estudante")
     */
    private $pagarItems;

    /**
     * @ORM\OneToMany(targetEntity=PagarActividade::class, mappedBy="estudante")
     */
    private $pagarActividades;

    /**
     * @ORM\OneToMany(targetEntity=PagarServico::class, mappedBy="estudante")
     */
    private $pagarServicos;

    /**
     * @ORM\OneToMany(targetEntity=SolicitarDocumento::class, mappedBy="estudante")
     */
    private $solicitarDocumentos;

    /**
     * @ORM\OneToMany(targetEntity=Mensalidade::class, mappedBy="estudante")
     */
    private $mensalidades;

    public function __construct()
    {
        $this->pagarItems = new ArrayCollection();
        $this->pagarActividades = new ArrayCollection();
        $this->pagarServicos = new ArrayCollection();
        $this->solicitarDocumentos = new ArrayCollection();
        $this->mensalidades = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getNomeFamilia(): ?string
    {
        return $this->nomeFamilia;
    }

    public function setNomeFamilia(string $nomeFamilia): self
    {
        $this->nomeFamilia = $nomeFamilia;

        return $this;
    }

    public function getGenero(): ?string
    {
        return $this->genero;
    }

    public function setGenero(string $genero): self
    {
        $this->genero = $genero;

        return $this;
    }

    public function getMorada(): ?string
    {
        return $this->morada;
    }

    public function setMorada(?string $morada): self
    {
        $this->morada = $morada;

        return $this;
    }

    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    public function setTelefone(string $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }

    public function getDataNasc(): ?\DateTimeInterface
    {
        return $this->dataNasc;
    }

    public function setDataNasc(\DateTimeInterface $dataNasc): self
    {
        $this->dataNasc = $dataNasc;

        return $this;
    }

    public function getClasse(): ?Classe
    {
        return $this->classe;
    }

    public function setClasse(Classe $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getResponsavel(): ?Responsavel
    {
        return $this->responsavel;
    }

    public function setResponsavel(?Responsavel $responsavel): self
    {
        $this->responsavel = $responsavel;

        return $this;
    }

    public function getNumeroBi(): ?string
    {
        return $this->numeroBi;
    }

    public function setNumeroBi(?string $numeroBi): self
    {
        $this->numeroBi = $numeroBi;

        return $this;
    }

    /**
     * @return Collection|PagarItem[]
     */
    public function getPagarItems(): Collection
    {
        return $this->pagarItems;
    }

    public function addPagarItem(PagarItem $pagarItem): self
    {
        if (!$this->pagarItems->contains($pagarItem)) {
            $this->pagarItems[] = $pagarItem;
            $pagarItem->setEstudante($this);
        }

        return $this;
    }

    public function removePagarItem(PagarItem $pagarItem): self
    {
        if ($this->pagarItems->removeElement($pagarItem)) {
            // set the owning side to null (unless already changed)
            if ($pagarItem->getEstudante() === $this) {
                $pagarItem->setEstudante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PagarActividade[]
     */
    public function getPagarActividades(): Collection
    {
        return $this->pagarActividades;
    }

    public function addPagarActividade(PagarActividade $pagarActividade): self
    {
        if (!$this->pagarActividades->contains($pagarActividade)) {
            $this->pagarActividades[] = $pagarActividade;
            $pagarActividade->setEstudante($this);
        }

        return $this;
    }

    public function removePagarActividade(PagarActividade $pagarActividade): self
    {
        if ($this->pagarActividades->removeElement($pagarActividade)) {
            // set the owning side to null (unless already changed)
            if ($pagarActividade->getEstudante() === $this) {
                $pagarActividade->setEstudante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PagarServico[]
     */
    public function getPagarServicos(): Collection
    {
        return $this->pagarServicos;
    }

    public function addPagarServico(PagarServico $pagarServico): self
    {
        if (!$this->pagarServicos->contains($pagarServico)) {
            $this->pagarServicos[] = $pagarServico;
            $pagarServico->setEstudante($this);
        }

        return $this;
    }

    public function removePagarServico(PagarServico $pagarServico): self
    {
        if ($this->pagarServicos->removeElement($pagarServico)) {
            // set the owning side to null (unless already changed)
            if ($pagarServico->getEstudante() === $this) {
                $pagarServico->setEstudante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SolicitarDocumento[]
     */
    public function getSolicitarDocumentos(): Collection
    {
        return $this->solicitarDocumentos;
    }

    public function addSolicitarDocumento(SolicitarDocumento $solicitarDocumento): self
    {
        if (!$this->solicitarDocumentos->contains($solicitarDocumento)) {
            $this->solicitarDocumentos[] = $solicitarDocumento;
            $solicitarDocumento->setEstudante($this);
        }

        return $this;
    }

    public function removeSolicitarDocumento(SolicitarDocumento $solicitarDocumento): self
    {
        if ($this->solicitarDocumentos->removeElement($solicitarDocumento)) {
            // set the owning side to null (unless already changed)
            if ($solicitarDocumento->getEstudante() === $this) {
                $solicitarDocumento->setEstudante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Mensalidade[]
     */
    public function getMensalidades(): Collection
    {
        return $this->mensalidades;
    }

    public function addMensalidade(Mensalidade $mensalidade): self
    {
        if (!$this->mensalidades->contains($mensalidade)) {
            $this->mensalidades[] = $mensalidade;
            $mensalidade->setEstudante($this);
        }

        return $this;
    }

    public function removeMensalidade(Mensalidade $mensalidade): self
    {
        if ($this->mensalidades->removeElement($mensalidade)) {
            // set the owning side to null (unless already changed)
            if ($mensalidade->getEstudante() === $this) {
                $mensalidade->setEstudante(null);
            }
        }

        return $this;
    }
}
