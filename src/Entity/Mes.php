<?php

namespace App\Entity;

use App\Repository\MesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MesRepository::class)
 */
class Mes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nome;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordem;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isLectivo;

    /**
     * @ORM\OneToMany(targetEntity=Mensalidade::class, mappedBy="mes")
     */
    private $mensalidades;

    public function __construct()
    {
        $this->mensalidades = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getOrdem(): ?int
    {
        return $this->ordem;
    }

    public function setOrdem(int $ordem): self
    {
        $this->ordem = $ordem;

        return $this;
    }

    public function getIsLectivo(): ?bool
    {
        return $this->isLectivo;
    }

    public function setIsLectivo(bool $isLectivo): self
    {
        $this->isLectivo = $isLectivo;

        return $this;
    }

    /**
     * @return Collection|Mensalidade[]
     */
    public function getMensalidades(): Collection
    {
        return $this->mensalidades;
    }

    public function addMensalidade(Mensalidade $mensalidade): self
    {
        if (!$this->mensalidades->contains($mensalidade)) {
            $this->mensalidades[] = $mensalidade;
            $mensalidade->setMes($this);
        }

        return $this;
    }

    public function removeMensalidade(Mensalidade $mensalidade): self
    {
        if ($this->mensalidades->removeElement($mensalidade)) {
            // set the owning side to null (unless already changed)
            if ($mensalidade->getMes() === $this) {
                $mensalidade->setMes(null);
            }
        }

        return $this;
    }
}
