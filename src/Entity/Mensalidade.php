<?php

namespace App\Entity;

use App\Repository\MensalidadeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MensalidadeRepository::class)
 */
class Mensalidade
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Mes::class, inversedBy="mensalidades")
     * @ORM\JoinColumn(nullable=false)
     */
    private $mes;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $borderaux;

    /**
     * @ORM\Column(type="float")
     */
    private $valor;

    /**
     * @ORM\ManyToOne(targetEntity=Estudante::class, inversedBy="mensalidades")
     * @ORM\JoinColumn(nullable=false)
     */
    private $estudante;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMes(): ?Mes
    {
        return $this->mes;
    }

    public function setMes(?Mes $mes): self
    {
        $this->mes = $mes;

        return $this;
    }

    public function getBorderaux(): ?string
    {
        return $this->borderaux;
    }

    public function setBorderaux(string $borderaux): self
    {
        $this->borderaux = $borderaux;

        return $this;
    }

    public function getValor(): ?float
    {
        return $this->valor;
    }

    public function setValor(float $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getEstudante(): ?Estudante
    {
        return $this->estudante;
    }

    public function setEstudante(?Estudante $estudante): self
    {
        $this->estudante = $estudante;

        return $this;
    }
}
