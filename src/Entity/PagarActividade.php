<?php

namespace App\Entity;

use App\Repository\PagarActividadeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PagarActividadeRepository::class)
 */
class PagarActividade
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Actividade::class, inversedBy="pagarActividades")
     * @ORM\JoinColumn(nullable=false)
     */
    private $actividade;

    /**
     * @ORM\ManyToOne(targetEntity=Estudante::class, inversedBy="pagarActividades")
     * @ORM\JoinColumn(nullable=false)
     */
    private $estudante;
    
    
    /**
     * @ORM\Column(type="float")
     */
    private $valor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActividade(): ?Actividade
    {
        return $this->actividade;
    }

    public function setActividade(?Actividade $activiade): self
    {
        $this->actividade = $activiade;

        return $this;
    }

    public function getEstudante(): ?Estudante
    {
        return $this->estudante;
    }

    public function setEstudante(?Estudante $estudante): self
    {
        $this->estudante = $estudante;

        return $this;
    }
    
    public function getValor(): ?float
    {
        return $this->valor;
    }

    public function setValor(?float $valor): self
    {
        $this->valor = $valor;

        return $this;
    }
}
