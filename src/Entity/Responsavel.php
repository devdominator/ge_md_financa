<?php

namespace App\Entity;

use App\Repository\ResponsavelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResponsavelRepository::class)
 */
class Responsavel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefone;

    /**
     * @ORM\OneToMany(targetEntity=Estudante::class, mappedBy="responsavel")
     */
    private $estudantes;

    public function __construct()
    {
        $this->estudantes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    public function setTelefone(?string $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }

    /**
     * @return Collection|Estudante[]
     */
    public function getEstudantes(): Collection
    {
        return $this->estudantes;
    }

    public function addEstudante(Estudante $estudante): self
    {
        if (!$this->estudantes->contains($estudante)) {
            $this->estudantes[] = $estudante;
            $estudante->setResponsavel($this);
        }

        return $this;
    }

    public function removeEstudante(Estudante $estudante): self
    {
        if ($this->estudantes->removeElement($estudante)) {
            // set the owning side to null (unless already changed)
            if ($estudante->getResponsavel() === $this) {
                $estudante->setResponsavel(null);
            }
        }

        return $this;
    }
}
