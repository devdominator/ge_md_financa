<?php

namespace App\Entity;

use App\Repository\SolicitarDocumentoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SolicitarDocumentoRepository::class)
 */
class SolicitarDocumento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Documento::class, inversedBy="solicitarDocumentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $documento;

    /**
     * @ORM\ManyToOne(targetEntity=Estudante::class, inversedBy="solicitarDocumentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $estudante;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $data;
    
    public function __construct() {
        $this->setData(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumento(): ?Documento
    {
        return $this->documento;
    }

    public function setDocumento(?Documento $documento): self
    {
        $this->documento = $documento;

        return $this;
    }

    public function getEstudante(): ?Estudante
    {
        return $this->estudante;
    }

    public function setEstudante(?Estudante $estudante): self
    {
        $this->estudante = $estudante;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(?\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }
}
