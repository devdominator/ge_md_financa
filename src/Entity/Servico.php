<?php

namespace App\Entity;

use App\Repository\ServicoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServicoRepository::class)
 */
class Servico
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descricao;

    /**
     * @ORM\Column(type="float")
     */
    private $preco;

    /**
     * @ORM\OneToMany(targetEntity=PagarServico::class, mappedBy="sevico")
     */
    private $pagarServicos;

    public function __construct()
    {
        $this->pagarServicos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(?string $descricao): self
    {
        $this->descricao = $descricao;

        return $this;
    }

    public function getPreco(): ?float
    {
        return $this->preco;
    }

    public function setPreco(float $preco): self
    {
        $this->preco = $preco;

        return $this;
    }

    /**
     * @return Collection|PagarServico[]
     */
    public function getPagarServicos(): Collection
    {
        return $this->pagarServicos;
    }

    public function addPagarServico(PagarServico $pagarServico): self
    {
        if (!$this->pagarServicos->contains($pagarServico)) {
            $this->pagarServicos[] = $pagarServico;
            $pagarServico->setSevico($this);
        }

        return $this;
    }

    public function removePagarServico(PagarServico $pagarServico): self
    {
        if ($this->pagarServicos->removeElement($pagarServico)) {
            // set the owning side to null (unless already changed)
            if ($pagarServico->getSevico() === $this) {
                $pagarServico->setSevico(null);
            }
        }

        return $this;
    }
}
