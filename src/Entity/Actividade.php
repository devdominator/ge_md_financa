<?php

namespace App\Entity;

use App\Repository\ActividadeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActividadeRepository::class)
 */
class Actividade
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $descricao;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $tipo;

    /**
     * @ORM\Column(type="float")
     */
    private $preco;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $data;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createAt;

    /**
     * @ORM\OneToMany(targetEntity=PagarActividade::class, mappedBy="actividade")
     */
    private $pagarActividades;

    public function __construct()
    {
        $this->pagarActividades = new ArrayCollection();
        $this->setCreateAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao): self
    {
        $this->descricao = $descricao;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getPreco(): ?float
    {
        return $this->preco;
    }

    public function setPreco(float $preco): self
    {
        $this->preco = $preco;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(?\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(?\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|PagarActividade[]
     */
    public function getPagarActividades(): Collection
    {
        return $this->pagarActividades;
    }

    public function addPagarActividade(PagarActividade $pagarActividade): self
    {
        if (!$this->pagarActividades->contains($pagarActividade)) {
            $this->pagarActividades[] = $pagarActividade;
            $pagarActividade->setActiviade($this);
        }

        return $this;
    }

    public function removePagarActividade(PagarActividade $pagarActividade): self
    {
        if ($this->pagarActividades->removeElement($pagarActividade)) {
            // set the owning side to null (unless already changed)
            if ($pagarActividade->getActiviade() === $this) {
                $pagarActividade->setActiviade(null);
            }
        }

        return $this;
    }
}
