<?php

namespace App\Entity;

use App\Repository\DocumentoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocumentoRepository::class)
 */
class Documento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $nome;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descricao;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $tipo;

    /**
     * @ORM\Column(type="float")
     */
    private $preco;

    /**
     * @ORM\OneToMany(targetEntity=SolicitarDocumento::class, mappedBy="documento")
     */
    private $solicitarDocumentos;

    public function __construct()
    {
        $this->solicitarDocumentos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(?string $descricao): self
    {
        $this->descricao = $descricao;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getPreco(): ?float
    {
        return $this->preco;
    }

    public function setPreco(float $preco): self
    {
        $this->preco = $preco;

        return $this;
    }

    /**
     * @return Collection|SolicitarDocumento[]
     */
    public function getSolicitarDocumentos(): Collection
    {
        return $this->solicitarDocumentos;
    }

    public function addSolicitarDocumento(SolicitarDocumento $solicitarDocumento): self
    {
        if (!$this->solicitarDocumentos->contains($solicitarDocumento)) {
            $this->solicitarDocumentos[] = $solicitarDocumento;
            $solicitarDocumento->setDocumento($this);
        }

        return $this;
    }

    public function removeSolicitarDocumento(SolicitarDocumento $solicitarDocumento): self
    {
        if ($this->solicitarDocumentos->removeElement($solicitarDocumento)) {
            // set the owning side to null (unless already changed)
            if ($solicitarDocumento->getDocumento() === $this) {
                $solicitarDocumento->setDocumento(null);
            }
        }

        return $this;
    }
}
