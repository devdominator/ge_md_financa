<?php

namespace App\Entity;

use App\Repository\PermissaoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PermissaoRepository::class)
 */
class Permissao
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $descricao;

    /**
     * @ORM\OneToMany(targetEntity=Utilizador::class, mappedBy="permissao")
     */
    private $utilizadors;

    public function __construct()
    {
        $this->utilizadors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(?string $descricao): self
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * @return Collection|Utilizador[]
     */
    public function getUtilizadors(): Collection
    {
        return $this->utilizadors;
    }

    public function addUtilizador(Utilizador $utilizador): self
    {
        if (!$this->utilizadors->contains($utilizador)) {
            $this->utilizadors[] = $utilizador;
            $utilizador->setGrupoUtilizador($this);
        }

        return $this;
    }

    public function removeUtilizador(Utilizador $utilizador): self
    {
        if ($this->utilizadors->removeElement($utilizador)) {
            // set the owning side to null (unless already changed)
            if ($utilizador->getGrupoUtilizador() === $this) {
                $utilizador->setGrupoUtilizador(null);
            }
        }

        return $this;
    }
}
