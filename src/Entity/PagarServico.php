<?php

namespace App\Entity;

use App\Repository\PagarServicoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PagarServicoRepository::class)
 */
class PagarServico
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Servico::class, inversedBy="pagarServicos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sevico;

    /**
     * @ORM\ManyToOne(targetEntity=Estudante::class, inversedBy="pagarServicos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $estudante;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSevico(): ?Servico
    {
        return $this->sevico;
    }

    public function setSevico(?Servico $sevico): self
    {
        $this->sevico = $sevico;

        return $this;
    }

    public function getEstudante(): ?Estudante
    {
        return $this->estudante;
    }

    public function setEstudante(?Estudante $estudante): self
    {
        $this->estudante = $estudante;

        return $this;
    }
}
