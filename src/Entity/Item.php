<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemRepository::class)
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descricao;

    /**
     * @ORM\Column(type="float")
     */
    private $preco;

    /**
     * @ORM\OneToMany(targetEntity=PagarItem::class, mappedBy="item")
     */
    private $pagarItems;

    public function __construct()
    {
        $this->pagarItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(?string $descricao): self
    {
        $this->descricao = $descricao;

        return $this;
    }

    public function getPreco(): ?float
    {
        return $this->preco;
    }

    public function setPreco(float $preco): self
    {
        $this->preco = $preco;

        return $this;
    }

    /**
     * @return Collection|PagarItem[]
     */
    public function getPagarItems(): Collection
    {
        return $this->pagarItems;
    }

    public function addPagarItem(PagarItem $pagarItem): self
    {
        if (!$this->pagarItems->contains($pagarItem)) {
            $this->pagarItems[] = $pagarItem;
            $pagarItem->setItem($this);
        }

        return $this;
    }

    public function removePagarItem(PagarItem $pagarItem): self
    {
        if ($this->pagarItems->removeElement($pagarItem)) {
            // set the owning side to null (unless already changed)
            if ($pagarItem->getItem() === $this) {
                $pagarItem->setItem(null);
            }
        }

        return $this;
    }
}
